﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class FaintButtonScript : MonoBehaviour
{

    void Start()
    {
        Cursor.visible = true;
        Screen.lockCursor = false;
    }

    public void ButtonContinue()
    {
        SceneManager.LoadScene(0);
    }

}
