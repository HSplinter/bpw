﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CutSceneScript : MonoBehaviour {

    public GameObject Cam1;
    public GameObject Cam2;
    public GameObject FPSCam;
    public GameObject Particles;
    public GameObject Model;
    public AudioSource CreepSound;

    void OnTriggerEnter(Collider other) {

        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        CreepSound.Play();
        Cam1.SetActive(true);
        FPSCam.SetActive(false);
        StartCoroutine(FinishCut());
    }

    IEnumerator FinishCut()
    {
        yield return new WaitForSeconds(4);
        Particles.SetActive(true);

        yield return new WaitForSeconds(4);
        Cam2.SetActive(true);
        Cam1.SetActive(false);
        yield return new WaitForSeconds(2);
        Model.SetActive(true);

        yield return new WaitForSeconds(8);
        //Cam2.SetActive(false);
        Model.SetActive(false);
        SceneManager.LoadScene(2);
    }
}
